﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="20008000">
	<Property Name="NI.Lib.ContainingLib" Type="Str">GraphBuilder.lvlib</Property>
	<Property Name="NI.Lib.ContainingLibPath" Type="Str">../../../GraphBuilder.lvlib</Property>
	<Property Name="NI.Lib.Description" Type="Str">-----------------------------------------------------------------------
BSD 3-Clause License

Copyright (c) 2020, Cyril GAMBINI
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5Q&lt;/07RB7W!,&lt;'&amp;&lt;9+K1,7Q,&lt;)%N&lt;!NMA3X)DW?-RJ(JQ"I\%%Z,(@`BA#==ZB3RN;]28_,V7@PWW`:R`&gt;HV*SU_WE@\N_XF[3:^^TX\+2YP)D7K6;G-RV3P)R`ZS%=_]J'XP/5N&lt;XH,7V\SEJ?]Z#5P?=J4HP+5JTTFWS%0?=B$DD1G(R/.1==!IT.+D)`B':\B'2Z@9XC':XC':XBUC?%:HO%:HO&amp;R7QT0]!T0]!S0I4&lt;*&lt;)?=:XA-(]X40-X40-VDSGC?"GC4N9(&lt;)"D2,L;4ZGG?ZH%;T&gt;-]T&gt;-]T?.S.%`T.%`T.)^&lt;NF8J4@-YZ$S'C?)JHO)JHO)R&gt;"20]220]230[;*YCK=ASI2F=)1I.Z5/Z5PR&amp;)^@54T&amp;5TT&amp;5TQO&lt;5_INJ6Z;"[(H#&gt;ZEC&gt;ZEC&gt;Z$"(*ETT*ETT*9^B)HO2*HO2*(F.&amp;]C20]C2)GN4UE1:,.[:/+5A?0^NOS?UJ^3&lt;*\9B9GT@7JISVW7*NIFC&lt;)^:$D`5Q9TWE7)M@;V&amp;D,6;M29DVR]6#R],%GC47T9_/=@&gt;Z5V&gt;V57&gt;V5E&gt;V5(OV?^T[FTP?\`?YX7ZRP6\D=LH%_8S/U_E5R_-R$I&gt;$\0@\W/VW&lt;[_"\Y[X&amp;],0^^+,]T_J&gt;`J@_B_]'_.T`$KO.@I"XC-_N!!!!!!</Property>
	<Property Name="NI.Lib.LocalName" Type="Str">NodeShape</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">536903680</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">)!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"./5F.31QU+!!.-6E.$4%*76Q!!$PA!!!27!!!!)!!!$NA!!!!J!!!!!B*(=G&amp;Q;%*V;7RE:8)O&lt;(:M;7)24G^E:6.I98"F,GRW9WRB=X-!!!!!!!#A)!#!!!!Q!!!I!!1!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!)^?F..)1:^"P4(A@1&amp;0NBQ!!!!-!!!!%!!!!!$'\KD&gt;WV.-4,8FYU@F\PC+V"W-W9]!MA4JA!G9\0B#@A!!!!!!!!!!\*F5O]5BUEGA4O#Q2I1]E!%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"$H9QD1CX[J)6T7WCL2:&amp;RJ!!!!"!!!!!!!!!!H!!&amp;-6E.$!!!!!1!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!)A!!!"RYH'.A9W"K9,D!!-3-1-T5Q01$S0Y!YD-!!'A"#$9!!!!!!%5!!!%9?*RD9-!%`Y%!3$%S-$#&gt;!.)M;/*A'M;G*M"F,C[\I/,-5$?S1E1:A7*-?Y!-*J!=KF[1@]"_1A&amp;M7-Q'!(&lt;V+"5!!!!!!!!-!!&amp;73524!!!!!!!$!!!"1Q!!!JRYH(.A:'$).,9Q;Q$3T%#MQ.$!E*S@EML&amp;!/1T1%!,%Q0&amp;)!"KHB;;O/'"QWF!I-=PXQ,G&gt;\OIM$48K0!QF@,^,V(B#(A"%GQ_QH'YWS0HO+-.7!F(&amp;E-71]$`Q)TG)TRAX=D[@629$!]U6#ITF!I@&lt;T2BB.A3#$7'Z@!/E-BB60O!?DA/0G4JH1A7[!32H3'-%M&gt;&gt;'(8%A/R?(M:!B$P1\!PL"LISCT%-*N`.&gt;NR"!]1_\C!#I4)A6!7%+A"2/U"%X'%1O@&lt;VP6WA='&amp;$#B-(+'Y!959%VG.A:!!ZHQE)1;&lt;__@``PQV1B!EKJAA6!\&amp;81NE;3(J/1M5=E/Q"[17:U!OE.;$MS6"W!^2&gt;),'L1,I!SLY$D5]1_T&amp;5(9D^#EA,1.E@A@1"+0M&lt;F.U!D1VUWNH@R25Z4=$3'Q$`F'K1!!!!!!YA!9!(!!!'-D!O-#YR!!!!!!!!$#!!A!!!!!1S-#YQ!!!!!!YA!9!(!!!'-D!O-#YR!!!!!!!!$#!!A!!!!!1S-#YQ!!!!!!YA!9!(!!!'-D!O-#YR!!!!!!!!&amp;!%!!!$V6T7#?3;CD#ZT5EY'34G&gt;!!!!$1!!!!!!!!!!!!!!!!!!!!!!!!#!`````Y!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A'!!!9'9!!''"A!"G!'!!;!!1!'Q!-!"L!.!!;-/Q!'A^5!"I#L!!;!V1!'A+M!"I$6!!;!KQ!'A.5!"G#O!!99W!!'"O!!"A'!!!@````]!!!1!````````````````````````````````````````````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!&amp;25!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!&amp;;REC+Q6!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!&amp;;RE1%"!1)CM&amp;1!!!!!!!!!!!!!!!!!!!!$``Q!!&amp;;RE1%"!1%"!1%#)L"5!!!!!!!!!!!!!!!!!!0``!)BE1%"!1%"!1%"!1%"!C+Q!!!!!!!!!!!!!!!!!``]!:'2!1%"!1%"!1%"!1%$`C!!!!!!!!!!!!!!!!!$``Q"EC)BE1%"!1%"!1%$```^E!!!!!!!!!!!!!!!!!0``!'3)C)C):%"!1%$``````W1!!!!!!!!!!!!!!!!!``]!:)C)C)C)C'3M````````:!!!!!!!!!!!!!!!!!$``Q"EC)C)C)C)C0````````^E!!!!!!!!!!!!!!!!!0``!'3)C)C)C)C)`````````W1!!!!!!!!!!!!!!!!!``]!:)C)C)C)C)D`````````:!!!!!!!!!!!!!!!!!$``Q"EC)C)C)C)C0````````^E!!!!!!!!!!!!!!!!!0``!'3)C)C)C)C)`````````W1!!!!!!!!!!!!!!!!!``]!C)C)C)C)C)D```````_)C!!!!!!!!!!!!!!!!!$``Q!!:'3)C)C)C0````_)L'1!!!!!!!!!!!!!!!!!!0``!!!!!'3)C)C)``_)C'1!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!"EC)C)C%!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!:%!!!!!!!!!!!!!!!!!!!!!!!!!!!0```````````````````````````````````````````Q!!!!Q!!5:13&amp;!!!!!!!!-!!!!%!!!!!!!!!I!!!!4J?*SNF%VI%U%5R^_%L5R#C\/RN6VJ3#T&lt;7-3#+'I.@B1\&amp;3KFK,(A)&gt;1%.XZ!;T1@YKG^,%)/P2D-1?AN"$RZ#-7\"#^\M#=&amp;?YA.?0&gt;3&amp;/RG@4PJ&lt;D[+]?)O$-MSP`&gt;`]`Z`"E$[TE9]$6AVA&lt;"&gt;`*AXQ;=:"+![37(`#&lt;]%NEB_!RF5C!H4&gt;*&amp;N?RJEV)2_T1D4UTQ00X#X^=V[";]*94OY^2"4M*D0B-/;-3T@5'N-@4?KZPO=KD)%W$JJ?/[KQ:^U85_B)/A4^CJ0EA91@FS3^,'&lt;C3&gt;*8&lt;8`?C?J)EJ[47$='%CLN2.9%;5`C**E#5IEZ:1%,$E"F5KF"=F.+#T;G,+:%J&lt;&gt;*%M^'$]X!BGV&gt;EIQ0M'ATK;DQY/.ANW\$87D2\D"%%8OB80EEK0&amp;A_;UY/LV/H+Y\H/04"B5;^?I1H?ULXU$N`4X1)"5\V(LC`87ZO5&amp;WQ7RVY^'D%=)C_*XV)34OO'*-]H2SE&amp;"W#!Z.FSS&lt;&lt;AO&lt;0$S0)/7$Y6`_-#F[FBA:DG8S3&lt;4I&gt;3$U0XF2#94?JJ_`$S249;U2$:RU+(,X&amp;$/WK?XR51Y9!AE?!&lt;R^GGHI&amp;QOYQ"Q&lt;;&amp;8%"V7;SYHC^.I\M3VVM2NV&gt;&lt;ELO,E_+]\$_XJD5=]4G;BF&gt;9TG&amp;;:&gt;4@5E&gt;:T`T_NZT&amp;%KVVJB2C_+TW3&gt;[%*N;5VBN1+R(IQ5]CM&gt;;26K$A[P&gt;*[]7";9Y[7G^:CM&gt;D"Y=QD&lt;FIF1JJJV&lt;?M07M0-YPDF3!/=:&amp;:]#[U&gt;5[X0_&lt;=SY6'67Z2Z?]&lt;W#S+L:EQR'@]H^A=6OOH=X37&lt;;#L?"U&gt;J2NUN`L:P8[K]S[J&lt;]&amp;N=ER^%[1D@Q#4^W&lt;D!!!!"!!!!#!!!!!%!!!!!!!!!!Q!!5*%3&amp;!!!!!!!!-!!!!%!!!!!!!!!')!!!"S?*RD9'$)%Z"A_M&gt;1^Z?"3?!LE#(^FY&amp;:U)`R.Q-$JZ`!93$.+#!*&amp;*&lt;^S]!OK!U7VD[CS]%!";JMD"S3()=&amp;/=!S(#U;$0```_@Y?O1;8-52(TB4::9]BQ1!&amp;'):!!!!!!!!"!!!!!=!!!)(!!!!"Q!!!#&amp;@&lt;GF@4'&amp;T&gt;%NO&lt;X&gt;O4X&gt;O;7ZH4&amp;:$&lt;'&amp;T=U.M&gt;8.U:8)!!!"#)!#!!!!!!!%!#!!Q`````Q!"!!!!!!!G!!!!!1!?1&amp;!!!":(=G&amp;Q;%*V;7RE:8)[4G^E:6.I98"F!!!"!!!!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!*3!!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!!!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ!!!!'3!!A!!!!!!"!!5!"Q!!!1!!WPV6Z1!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!!!!!:)!#!!!!!!!%!"1!(!!!"!!$;`68F!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9Q!!!$QA!)!!!!!!!1!)!$$`````!!%!!!!!!#!!!!!"!"B!5!!!%5ZP:'64;'&amp;Q:3ZM&gt;G.M98.T!!%!!!!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF!!!!'3!!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!!!!!54EEO4&amp;9O17RM,F.P&gt;8*D:5^O&lt;(E!!!!6)!#!!!!!!!%!"!!B!!%!!!%!!!!!!!!!!!1!!A!)!!!!"!!!!%!!!!!I!!!!!A!!"!!!!!!#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!/E!!!&amp;G?*S.DT^,!U%1R8_&lt;D?;@U347QB976D:_A5."OR!-[6VP^]T"QBVX?](3TWFPLR^!=(+*J,#2"]/].T0P\1,HJ(S^@[]_!$V\K'SZPGXSY(RV(49B@Z\/#_?8;VN[Y7GQ&gt;4U_+'E-8#5,O,A,42V^:9L-N%OGL0+.D&gt;YY'SV(!I9Y5*]I)8J0O:&amp;WE04178CBH\TJYD(O"&amp;^E4-2&lt;S`IR0&gt;WYD/%W3]&gt;8*^=,/P1:=0GP`![ND23&amp;H$V*&gt;*&gt;\-4'=-":RCV-*_Y8;Y[^SG*SVW(V%-2&amp;(Z%&amp;)7&amp;@5%6.G5E=`+BQ]O1!!!!!!!'5!!1!#!!-!"!!!!%A!$Q1!!!!!$Q$:!.1!!!"2!!]%!!!!!!]!W1$5!!!!7A!0"!!!!!!0!.E!V!!!!'/!!)1!A!!!$Q$:!.1)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65E"-A!!!&amp;*45E-.#A!$4&amp;:$1UR#6F=!!!\Y!!!%6A!!!#!!!!\9!!!!!!!!!!!!!!!A!!!!.!!!"%A!!!!&gt;4%F#4A!!!!!!!!&amp;M4&amp;:45A!!!!!!!!'!5F242Q!!!!!!!!'51U.46!!!!!!!!!'I4%FW;1!!!!!!!!']1U^/5!!!!!!!!!(16%UY-!!!!!!!!!(E2%:%5Q!!!!!!!!(Y4%FE=Q!!!!!!!!)-6EF$2!!!!!!!!!)A&gt;G6S=Q!!!!1!!!)U5U.45A!!!!!!!!+92U.15A!!!!!!!!+M35.04A!!!!!!!!,!;7.M/!!!!!!!!!,54%FG=!!!!!!!!!,I2F"&amp;?!!!!!!!!!,]2F")9A!!!!!!!!-12F"421!!!!!!!!-E6F"%5!!!!!!!!!-Y4%FC:!!!!!!!!!.-1E2&amp;?!!!!!!!!!.A1E2)9A!!!!!!!!.U1E2421!!!!!!!!/)6EF55Q!!!!!!!!/=2&amp;2)5!!!!!!!!!/Q466*2!!!!!!!!!0%3%F46!!!!!!!!!096E.55!!!!!!!!!0M2F2"1A!!!!!!!!1!!!!!!0````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!Q!!!!!!!!!!$`````!!!!!!!!!.1!!!!!!!!!!0````]!!!!!!!!![!!!!!!!!!!!`````Q!!!!!!!!$Q!!!!!!!!!!$`````!!!!!!!!!2Q!!!!!!!!!!0````]!!!!!!!!"*!!!!!!!!!!!`````Q!!!!!!!!&amp;-!!!!!!!!!!$`````!!!!!!!!!:A!!!!!!!!!!0````]!!!!!!!!"K!!!!!!!!!!%`````Q!!!!!!!!,Q!!!!!!!!!!@`````!!!!!!!!!Q1!!!!!!!!!#0````]!!!!!!!!$&amp;!!!!!!!!!!*`````Q!!!!!!!!-I!!!!!!!!!!L`````!!!!!!!!!TA!!!!!!!!!!0````]!!!!!!!!$4!!!!!!!!!!!`````Q!!!!!!!!.E!!!!!!!!!!$`````!!!!!!!!!XA!!!!!!!!!!0````]!!!!!!!!$`!!!!!!!!!!!`````Q!!!!!!!!A!!!!!!!!!!!$`````!!!!!!!!#"!!!!!!!!!!!0````]!!!!!!!!)'!!!!!!!!!!!`````Q!!!!!!!!K=!!!!!!!!!!$`````!!!!!!!!#K1!!!!!!!!!!0````]!!!!!!!!+L!!!!!!!!!!!`````Q!!!!!!!!K]!!!!!!!!!!$`````!!!!!!!!#M1!!!!!!!!!!0````]!!!!!!!!,,!!!!!!!!!!!`````Q!!!!!!!!MU!!!!!!!!!!$`````!!!!!!!!$5!!!!!!!!!!!0````]!!!!!!!!.3!!!!!!!!!!!`````Q!!!!!!!!V1!!!!!!!!!!$`````!!!!!!!!$8Q!!!!!!!!!A0````]!!!!!!!!/&lt;!!!!!!.4G^E:6.I98"F,G.U&lt;!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!B*(=G&amp;Q;%*V;7RE:8)O&lt;(:M;7)24G^E:6.I98"F,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!!!!!!!"!!%!!!!!!!!!!!!!!1!91&amp;!!!"&amp;/&lt;W2F5WBB='5O&lt;(:D&lt;'&amp;T=Q!"!!!!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!&amp;1#!!!!!!!!!!!!!!!!</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.0</Property>
	<Item Name="NodeShape.ctl" Type="Class Private Data" URL="NodeShape.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
</LVClass>
